<?php

new BarcodeCheckinAdmin();

class BarcodeCheckinAdmin {

private $nonce = 'check-in-admin';
private $api;

function __construct()
{
    require_once(dirname(__FILE__).'/class-adminajax.php');
    add_action("admin_init", array(&$this, "admin_init"));
    add_action("admin_menu", array(&$this, "admin_menu"));
    add_action("admin_footer", array(&$this, 'admin_footer'));

    $ajax = new WP_AdminAjax(
        "check-in",
        array(&$this, "checkin_result"),
        false,
        $this->nonce
    );
    $this->api = $ajax->get_url();
}

public function admin_init()
{
}

public function admin_footer()
{
    printf(
        "<script type=\"text/javascript\">var api = '%s'</script>;",
        $this->api
    );
}

public function checkin_result()
{
    nocache_headers();
    header('Content-type: application/json; charset=utf-8');

    $regex = "^[a-z0-9]{".BarcodeCheckIn::key_length."}$";
    if (isset($_GET['id']) && preg_match("/^[a-z0-9]+$/", $_GET['id'])) {
        $uid = BarcodeCheckIn::get_user_by_key($_GET['id']);
        if ($uid) {
            $user = get_userdata($uid);
            $user->avatar = get_avatar($user->user_email, 256);
            global $wp_roles;
            $roles = array_values($user->roles);
            $user->member_type = $wp_roles->roles[$roles[0]]['name'];
            $user->history = get_history($user->ID);
            checkin($user->ID);
            $user->stayed = get_last($user->ID);
            echo json_encode($user);
            exit;
        }
    }

    echo json_encode(array());
    exit;
}

public function admin_menu()
{
    global $pagenow;
    if (current_user_can('front_staff') && ($pagenow !== 'admin.php' || !isset($_GET['page']) || $_GET['page'] !== 'check-in')) {
        wp_redirect(admin_url('admin.php?page=check-in'));
        exit;
    } elseif(current_user_can('member')) {
        wp_redirect(home_url('check-in'));
        exit;
    }
    $hook = add_menu_page(
        "Check-in",
        "Check-in",
        "check-in",
        "check-in",
        array(&$this, "admin_panel"),
        BC_CHECKIN_URI.'/img/barcode-16x16.png'
    );
    add_action('admin_print_styles-'.$hook, array(&$this, 'admin_styles'));
    add_action('admin_head-'.$hook, array(&$this, 'admin_head'));
}

public function admin_panel()
{
    echo '<div id="checkin-admin" class="">';
    echo '<h2>Welcome to '.get_bloginfo('name').'</h2>';
    printf(
        '<p class="lead">お客様に %s にスマートフォンでアクセスしていただいて、表示されたバーコードを読み込んでください。</p>',
        home_url('check-in')
    );
    printf(
        '<input type="hidden" id="nonce" name="nonce" value="%s" />',
        esc_attr(wp_create_nonce($this->nonce))
    );
?>
<input type="text" id="barcode" value="" style="ime-mode:disabled;" />
<div id="alert" class="alert alert-danger" style="display:none;"></div>

<div id="user-info" class="row-fluid" style="display:none; width:100%; margin:2em 0;">
<div class="well" style="overflow:auto;">
<div id="user_avatar" class="span3">
</div>
<div class="span9">
<h3 id="display_name" style="border:none;"></h3>
<table class="table">
    <tr>
        <th style="width:25%;">メンバー種別</th>
        <td id="member_type"></td>
    </tr>
    <tr>
        <th>利用時間</th>
        <td id="stayed"></td>
    </tr>
    <tr>
        <th>履歴</th>
        <td><ul id="history" style="list-style-type:square;"></ul></td>
    </tr>
</table>
</div>
</div><!-- end .well -->
</div><!-- end #user-info -->

</div><!-- #check-in-admin -->
<?php
}

public function admin_styles()
{
    wp_register_style(
        'check-in-admin-style',
        BC_CHECKIN_URI.'/css/admin.css',
        array(),
        filemtime(BC_CHECKIN_DIR.'/css/admin.css')
    );
    wp_enqueue_style("check-in-admin-style");
    wp_register_script(
        'check-in-admin-script',
        BC_CHECKIN_URI.'/js/admin.js',
        array('jquery'),
        filemtime(BC_CHECKIN_DIR.'/js/admin.js'),
        true
    );
    wp_enqueue_script("check-in-admin-script");
}

public function admin_head()
{
}


} // class BarcodeCheckinAdmin()

// eof
