<?php

function checkin($uid){
    global $wpdb;
    $table = $wpdb->prefix.CHECKIN_TABLE;

    // 重複チェックイン防止のため過去1分位内のチェックインは削除
    $cancel = current_time('timestamp') - 60;
    $sql = 'delete from `'.$table.'` where user_id=%d and checkined > %s';
    $wpdb->query($wpdb->prepare($sql, $uid, $cancel));

    $sql = 'insert into `'.$table.'` values(null, %d, %d)';
    $wpdb->query($wpdb->prepare($sql, $uid, current_time('timestamp')));
}

function get_history($uid){
    global $wpdb;
    $table = $wpdb->prefix.CHECKIN_TABLE;

    $sql = 'select `checkined` from `'.$table.'` where user_id=%d order by `id` desc limit 0,5';
    $res = $wpdb->get_col($wpdb->prepare($sql, $uid), 0);

    $hists = array();
    foreach ($res as $h) {
        $hists[] = date_i18n('Y-m-d H:i:s', $h);
    }
    return $hists;
}

function get_last($uid){
    global $wpdb;
    $table = $wpdb->prefix.CHECKIN_TABLE;

    $sql = 'select `checkined` from `'.$table.'` where user_id=%d order by `id` desc limit 0,2';
    $res = $wpdb->get_col($wpdb->prepare($sql, $uid), 0);

    if (date_i18n('Y-m-d', $res[0]) === date_i18n('Y-m-d', $res[1])) {
        return date('H:i', $res[0] - $res[1]);
    } else {
        return '';
    }
}

